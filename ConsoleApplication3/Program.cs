﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplication3
{
	class Program
	{
		static void Main(string[] args)
		{
			SmtpClient mailClient = new SmtpClient("smtp.qq.com");
			mailClient.EnableSsl = true;
			//Credentials登陆SMTP服务器的身份验证.
			mailClient.Credentials = new NetworkCredential("1213250243@qq.com", "");
			//test@qq.com发件人地址、test@tom.com收件人地址
			MailMessage message = new MailMessage(new MailAddress("1213250243@qq.com"), new MailAddress("aladdingame@qq.com"));

			// message.Bcc.Add(new MailAddress("tst@qq.com")); //可以添加多个收件人
			message.Body = "Hello Word!";//邮件内容
			message.Subject = "this is a test";//邮件主题
			//Attachment 附件
			Attachment att = new Attachment(@"D:/test.mp3");
			message.Attachments.Add(att);//添加附件
			Console.WriteLine("Start Send Mail....");
			//发送....
			mailClient.Send(message);

			Console.WriteLine("Send Mail Successed");

			Console.ReadLine();
		}
	}
}
